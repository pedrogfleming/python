from character import Character
class Arena(object):
    """description of class"""

    def __init__(self,name,list_players,list_enemies):
        self.name = name
        self.list_players = list_players
        self.list_enemies = list_enemies
    def add_player(self,p):
            self.list_players.append(p)
    def add_enemy(self,e):
            self.list_enemies.append(e)
    def initiate_fight(p,e):
            e.inititate_defense(p.initiate_attack())
            if Arena.action_fight(p,e):
                return f"{p.name} has dealed damaged to {e.name}"
            elif Arena.action_fight(e,p):
                return f"{e.name} has dealed damaged to {p.name}"
    def action_fight(a,d):
        return d.inititate_defense(a.initiate_attack())
            

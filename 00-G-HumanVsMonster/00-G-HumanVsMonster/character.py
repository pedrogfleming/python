import random


class Character(object):
    """A class of character who could die and fight for his king"""
    #initialize function
    def __init__(self,name,hp,min_attack,max_attack):
        self.name = name
        self.hp = hp
        self.min_attack = min_attack
        self.max_attack = max_attack
        self.is_alive = True

    def show_stats(self):
        """return(self.name+' '+str(self.hp)+' '+str(self.min_attack)+' '+str(self.max_attack)+' '+str(self.is_alive))"""         
        return f"Name {self.name} Hp {self.hp} Min Attack {self.min_attack} Max Attack {self.max_attack} Min Attack {self.is_alive}"

    def initiate_attack(self):
        return random.randint(self.min_attack,self.max_attack)

    def inititate_defense(self,incoming_damage):
        #to_dodge = bool(random.randint(0,1))
        
        if not random.choice([True, False]):            
            self.hp -= incoming_damage
            if self.hp <= 0:
                self.is_alive =False
            return True
        else:
            return False

